package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public double getTfahrenheit(String celsius){
        double tcelsius=Double.parseDouble(celsius);
        double tfahrenheit=tcelsius*9/5+32;
        return tfahrenheit;
    }
}
